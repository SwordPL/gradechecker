require 'data_mapper'
require 'dm-validations'
require 'bcrypt'

class User
  include DataMapper::Resource
  include BCrypt

  property :id, Serial, key: true
  property :username, String, length: 3..24, unique: true, required: true
  property :password, BCryptHash, required: true
  attr_accessor :password_confirmation
  property :name, String, required: true
  property :surname, String, required: true

  validates_confirmation_of :password

  def authenticate(attempted_password)
    if self.password == attempted_password
      true
    else
      false
    end
  end

end

DataMapper.finalize
DataMapper.auto_upgrade!