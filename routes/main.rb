class TheApp < Sinatra::Application
  get '/' do
    erb :index
  end

  get '/results' do
    env['warden'].authenticate!
    @user = env['warden'].user
    erb(:results)
  end
end
