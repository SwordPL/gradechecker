class TheApp < Sinatra::Application
  get '/login/form' do
    erb :login_form
  end

  post '/login/form' do
    session[:return_to] ||= '/'
    env['warden'].authenticate!
    flash[:success] = env['warden'].message

    client = settings.google_client
    redirect client.authorization.authorization_uri
    #redirect :results
  end

  get '/oauth2callback' do
    authorization_code = params['code']

    client = settings.google_client
    client.authorization.code = authorization_code
    client.authorization.fetch_access_token!

    oauth_token = client.authorization.access_token

    session[:oauth_token] = oauth_token

    redirect :results
  end

  get '/logout' do
    env['warden'].raw_session.inspect
    env['warden'].logout
    flash[:success] = 'Sucessfully logged out'
    redirect '/'
  end

  post '/login/unauthenticated' do
    session[:return_to] = env['warden.options'][:attempted_path]
    puts env['warden.options'][:attempted_path]
    flash[:error] = env['warden'].message || 'You must log in'
    redirect '/login/form'
  end

  get '/login/register' do
    erb :login_register
  end

  post '/login/regattempt' do
    previous_url = session[:previous_url] || '/'

    u = User.new
    u.username = params['username'].to_s
    u.password = params['password'].to_s
    u.password_confirmation = params['password_confirmation'].to_s
    u.name= params['name'].to_s
    u.surname = params['surname'].to_s
    u.save

    @error = create_error_string(u.errors)
    # session[:identity] = u.name+' '+u.surname
    redirect to previous_url
  end

end