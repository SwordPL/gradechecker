class TheApp
  use Warden::Manager do |config|
    config.serialize_into_session { |user| user.id }
    config.serialize_from_session { |id| User.get(id) }

    config.scope_defaults :default,
                          strategies: [:password],
                          action: 'login/unauthenticated'

    config.failure_app = self
  end

  Warden::Manager.before_failure do |env, _|
    env['REQUEST_METHOD'] = 'POST'
  end

  Warden::Strategies.add(:password) do
    def valid?
      params['user'] && params['user']['username'] && params['user']['password']
    end

    def authenticate!
      user = User.first(username: params['user']['username'])
      if user.nil?
        fail! 'The username does not exist'
      elsif user.authenticate(params['user']['password'])
        p params['user']['password']
        success! user
      else
        fail! 'Could not sign in!'
      end
    end
  end
end