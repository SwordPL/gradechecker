require 'google/api_client'
require 'google_drive'
module GoogleDriveHelper

  def print_results(username)
    w = String.new
    ws = get_spreadsheet
    ws.worksheets.each do |x|
      next if no_of_student_row(username, x) == -1
      w += '<h3>'+x.title+'</h3>'
      w += print_grades(username, x)
      w += '<br/>'
    end
    w
  end

  def no_of_student_row(username, ws)
    ws.rows(1).each_with_index do |x, i|
      if x[0] == username
        return i
      end
    end
    -1
  end

  def print_grades(username, ws)
    return '' unless ws
    row = no_of_student_row(username, ws)

    sum = 0.0
    u = String.new
    u += '<table class="table table-striped table-bordered">'
    u += '<tr>'

    (2..ws.max_cols).each { |i|
      u += '<td>'+ws[1, i].to_s+'</td>'
    }
    u += '<td class="info">Average</td></tr>'

    (1...ws.max_cols).each do |i|
      u += '<td>'+ws.rows(1)[row][i].to_s+'</td>'
      sum += ws.rows(1)[row][i].to_f
    end
    u += '<td>'+((sum)/(ws.max_cols-1)).to_f.round(2).to_s+'</td>'
    u += '</tr>'
    u += '</table>'
    u
  end

  def get_spreadsheet
    client = settings.google_client
    client.authorization.access_token = session[:oauth_token] if session[:oauth_token]
    google_session = GoogleDrive.login_with_oauth(session[:oauth_token])
    google_session.spreadsheet_by_key('1zu_8B6w7uzCP1x0OPI0gk8T6REOhua4IHJ5RPHT3-J8')
  end
end