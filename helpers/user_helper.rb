module UserHelper
  def username
    u = env['warden'].user
    u ? 'Hello, '+ u.name+ ' '+u.surname : 'Hello, stranger!'
  end

  def create_error_string(arr)
    arr.inject { |sum, e| sum + e.to_s + "\n" }
  end

end