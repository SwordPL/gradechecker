require 'bundler'
Bundler.require

class TheApp < Sinatra::Application
  register Sinatra::Flash


  configure do
    google_client = Google::APIClient.new
    auth = google_client.authorization
    auth.client_id = '429738314128-ns1te10u1uh8d1ikq0j6i2ivo4slm3sb.apps.googleusercontent.com'
    auth.client_secret = 'BHdopzxqePSwHCZXK2pP8-wU'
    auth.scope =
        'https://www.googleapis.com/auth/drive ' +
            'https://spreadsheets.google.com/feeds/'
    auth.redirect_uri = 'http://localhost:9292/oauth2callback'

    set :google_client, google_client
    set :google_client_driver, google_client.discovered_api('drive', 'v2')
  end

  enable :sessions

  DataMapper.setup(:default, "sqlite3://#{Dir.pwd}/dev.db")
  DataMapper.auto_upgrade!

  helpers do
    include Rack::Utils
    alias_method :h, :escape_html
  end

end

require_relative 'models/init'
require_relative 'helpers/init'
require_relative 'routes/init'